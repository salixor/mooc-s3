﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using TeddyMineExplosion;

namespace ProgrammingAssignment5
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        const int WindowWidth = 800;
        const int WindowHeight = 600;

        Random rand = new Random();

        // sprites
        Texture2D mineSprite;
        Texture2D teddySprite;
        Texture2D explosionSprite;

        // objects
        List<TeddyBear> teddys = new List<TeddyBear>();
        List<Mine> mines = new List<Mine>();
        List<Explosion> explosions = new List<Explosion>();

        // click processing
        bool leftClickStarted = false;
        bool leftButtonReleased = true;

        // delay support
        int totalDelayMilliseconds = 0;
        int elapsedDelayMilliseconds = 0;

        // random velocity
        double xVelocity;
        double yVelocity;
        Vector2 randomVelocity;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // set resolution and make mouse visible
            graphics.PreferredBackBufferWidth = WindowWidth;
            graphics.PreferredBackBufferHeight = WindowHeight;
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            totalDelayMilliseconds = (1 + rand.Next(2)) * 1000;
            xVelocity = rand.NextDouble() - 0.5;
            yVelocity = rand.NextDouble() - 0.5;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // load mine and teddy bear sprites
            mineSprite = Content.Load<Texture2D>(@"graphics\mine");
            teddySprite = Content.Load<Texture2D>(@"graphics\teddybear");
            explosionSprite = Content.Load<Texture2D>(@"graphics\explosion");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // get current mouse state
            MouseState mouse = Mouse.GetState();

            // check for left click started
            if (mouse.LeftButton == ButtonState.Pressed && leftButtonReleased)
            {
                leftClickStarted = true;
                leftButtonReleased = false;
            }
            else if (mouse.LeftButton == ButtonState.Released)
            {
                leftButtonReleased = true;

                // if left click finished, add new mine to list
                if (leftClickStarted)
                {
                    leftClickStarted = false;

                    // add a new mine to the end of the list of mines
                    mines.Add(new Mine(mineSprite, mouse.X, mouse.Y));
                }
            }

            // update timer
            elapsedDelayMilliseconds += gameTime.ElapsedGameTime.Milliseconds;
            if (elapsedDelayMilliseconds >= totalDelayMilliseconds)
            {
                elapsedDelayMilliseconds = 0;
                totalDelayMilliseconds = (1 + rand.Next(2)) * 1000;

                xVelocity = rand.NextDouble() - 0.5;
                yVelocity = rand.NextDouble() - 0.5;
                randomVelocity = new Vector2((float)xVelocity, (float)yVelocity);
                TeddyBear teddy = new TeddyBear(teddySprite, randomVelocity, WindowWidth, WindowHeight);

                teddys.Add(teddy);
            }

            // check for collisions
            foreach (TeddyBear teddy in teddys)
            {
                foreach (Mine mine in mines)
                {
                    if (teddy.CollisionRectangle.Intersects(mine.CollisionRectangle))
                    {
                        Explosion explosion = new Explosion(
                            explosionSprite,
                            mine.CollisionRectangle.X + mine.CollisionRectangle.Width / 2,
                            mine.CollisionRectangle.Y + mine.CollisionRectangle.Height / 2
                        );
                        explosions.Add(explosion);
                        teddy.Active = false;
                        mine.Active = false;
                    }

                }
            }

            // update teddys and explosions
            foreach (TeddyBear teddy in teddys)
                teddy.Update(gameTime);

            foreach (Explosion explosion in explosions)
                explosion.Update(gameTime);

            // delete teddys, mines and explosions appropriately
            for (int i = teddys.Count - 1; i >= 0; i--)
                if (!teddys[i].Active)
                    teddys.RemoveAt(i);

            for (int i = mines.Count - 1; i >= 0; i--)
                if (!mines[i].Active)
                    mines.RemoveAt(i);

            for (int i = explosions.Count - 1; i >= 0; i--)
                if (!explosions[i].Playing)
                    explosions.RemoveAt(i);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            // draw each teddy bear
            foreach (TeddyBear teddy in teddys)
                teddy.Draw(spriteBatch);

            // draw each mine
            foreach (Mine mine in mines)
                mine.Draw(spriteBatch);

            // draw each explosion
            foreach (Explosion explosion in explosions)
                explosion.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
