﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace OptionalProject
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // useful constants
        int WindowWidth = 800;
        int WindowHeight = 600;

        // game state
        GameState gameState = GameState.Menu;

        // Increment 1: opening screen fields
        Texture2D openingScreenImage;
        Rectangle openingScreen;

        // Increment 2: board field
        NumberBoard board;

        // Increment 5: random field
        Random random = new Random();

        // Increment 5: new game sound effect field
        SoundEffect winSound;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Increment 1: set window resolution and make mouse visible
            graphics.PreferredBackBufferWidth = WindowWidth;
            graphics.PreferredBackBufferHeight = WindowHeight;
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Increment 1: load opening screen and set opening screen draw rectangle
            openingScreenImage = Content.Load<Texture2D>(@"graphics\openingscreen");
            openingScreen = new Rectangle(0, 0, WindowWidth, WindowHeight);

            // Increment 5: create a board
            StartGame();

            // Increment 5: load new game sound effect
            winSound = Content.Load<SoundEffect>(@"audio\applause");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            MouseState mouse = Mouse.GetState();
            KeyboardState keyboard = Keyboard.GetState();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || keyboard.IsKeyDown(Keys.Escape))
                Exit();

            // Increment 2: change game state if game state is GameState.Menu and user presses Enter
            if (gameState == GameState.Menu && keyboard.IsKeyDown(Keys.Enter))
                gameState = GameState.Playing;

            // Increment 4: if we're actually playing, update mouse state and update board
            if (gameState == GameState.Playing || gameState == GameState.Ending)
                gameState = board.Update(gameTime, mouse, keyboard, gameState);

            // Increment 5: check for correct guess
            if (gameState == GameState.Ended && keyboard.IsKeyDown(Keys.Space)) {
                //winSound.Play((float) 0.2, 0, 0);
                StartGame();
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();

            // Increments 1 and 2: draw appropriate items here
            if (gameState == GameState.Menu)
                spriteBatch.Draw(openingScreenImage, openingScreen, Color.White);
            else
                board.Draw(spriteBatch);

            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Starts a game
        /// </summary>
        void StartGame()
        {
            if (gameState != GameState.Menu)
                gameState = GameState.Playing;

            // Increment 5: randomly generate new number for game
            int correctNumber = 1 + random.Next(9);

            // Increment 5: create the board object
            int sideLength = Math.Min(WindowHeight, WindowWidth);
            Vector2 center = new Vector2(WindowWidth / 2, WindowHeight / 2);
            board = new NumberBoard(Content, center, sideLength, correctNumber);
        }
    }
}
