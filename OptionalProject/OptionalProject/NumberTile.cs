using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace OptionalProject
{
    /// <remarks>
    /// A number tile
    /// </remarks>
    class NumberTile
    {
        #region Fields

        // original length of each side of the tile and position
        int originalSideLength;
        int originalX;
        int originalY;

        // whether or not this tile is the correct number
        int number;
        bool isCorrectNumber;

        // drawing support
        Texture2D normalTexture;
        Rectangle drawRectangle;
        Rectangle sourceRectangle;

        // field for blinking tile texture
        Texture2D blinkingTexture;

        // field for current texture
        Texture2D currentTexture;

        // blinking support
        const int TotalBlinkMilliseconds = 2000;
        int elapsedBlinkMilliseconds = 0;
        const int FrameBlinkMilliseconds = 100;
        int elapsedFrameMilliseconds = 0;

        // shrinking support
        const int TotalShrinkMilliseconds = 1000;
        int elapsedShrinkMilliseconds;

        // fields to keep track of visible, blinking, and shrinking
        bool isVisible = true;
        bool isBlinking = false;
        bool isShrinking = false;

        // click support
        bool clickStarted;
        bool buttonReleased;

        // sound effect field
        SoundEffect sound;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="contentManager">the content manager</param>
        /// <param name="center">the center of the tile</param>
        /// <param name="sideLength">the side length for the tile</param>
        /// <param name="number">the number for the tile</param>
        /// <param name="correctNumber">the correct number</param>
        public NumberTile(ContentManager contentManager, Vector2 center, int sideLength,
            int number, int correctNumber)
        {
            // load content for the tile and create draw rectangle
            LoadContent(contentManager, number);
            drawRectangle = new Rectangle(
                (int) (center.X - sideLength / 2),
                (int) (center.Y - sideLength / 2),
                sideLength,
                sideLength
            );

            // set original side length and positon fields
            this.originalSideLength = sideLength;
            this.originalX = drawRectangle.X;
            this.originalY = drawRectangle.Y;

            // set isCorrectNumber flag
            this.number = number;
            isCorrectNumber = number == correctNumber;

            // load sound effect field to correct or incorrect sound effect
            sound = contentManager.Load<SoundEffect>((isCorrectNumber) ? @"audio\explosion" : @"audio\loser");
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Updates the tile based on game time and mouse state
        /// </summary>
        /// <param name="gameTime">the current GameTime</param>
        /// <param name="mouse">the current mouse state</param>
        /// <param name="keyboard">the current keyboard state</param>
        /// <param name="gameState">the current game state</param>
        /// <return>true if the correct number was guessed, false otherwise</return>
        public GameState Update(GameTime gameTime, MouseState mouse, KeyboardState keyboard, GameState gameState)
        {
            // Increment 4: add code to highlight/unhighlight the tile
            // Increments 4 and 5: add code for shrinking and blinking support
            if (isBlinking) {
                currentTexture = blinkingTexture;

                elapsedBlinkMilliseconds += gameTime.ElapsedGameTime.Milliseconds;
                if (elapsedBlinkMilliseconds <= TotalBlinkMilliseconds) {
                    elapsedFrameMilliseconds += gameTime.ElapsedGameTime.Milliseconds;
                    if (elapsedFrameMilliseconds <= FrameBlinkMilliseconds) {
                        sourceRectangle.X = (sourceRectangle.X == currentTexture.Width / 2) ? currentTexture.Width / 2 : 0;
                    }
                    else {
                        sourceRectangle.X = (sourceRectangle.X == currentTexture.Width / 2) ? 0 : currentTexture.Width / 2;
                        elapsedFrameMilliseconds = 0;
                    }
                }
                else {
                    isShrinking = true;
                    isBlinking = false;
                }
            }
            else if (isShrinking) {
                sourceRectangle.X = (currentTexture == normalTexture) ? currentTexture.Width / 2 : 0;

                elapsedShrinkMilliseconds += gameTime.ElapsedGameTime.Milliseconds;
                float newSideLength = originalSideLength * (TotalShrinkMilliseconds - elapsedShrinkMilliseconds) / TotalShrinkMilliseconds;
                if (newSideLength > 0) {
                    drawRectangle.Width = (int) newSideLength;
                    drawRectangle.Height = (int) newSideLength;
                    drawRectangle.X = (int) (originalX + (originalSideLength - newSideLength) / 2);
                    drawRectangle.Y = (int) (originalY + (originalSideLength - newSideLength) / 2);
                }
                else {
                    isVisible = false;
                }
            }
            else {
                if (drawRectangle.Contains(mouse.Position) || keyboard.IsKeyDown(ConvertIntToKeyboardKey(number))) {
                    if ( (mouse.LeftButton == ButtonState.Pressed || keyboard.IsKeyDown(ConvertIntToKeyboardKey(number)))
                      && !clickStarted && !buttonReleased && gameState == GameState.Playing) {
                        clickStarted = true;
                        buttonReleased = false;
                    }
                    if ( (mouse.LeftButton == ButtonState.Released || keyboard.IsKeyUp(ConvertIntToKeyboardKey(number)))
                      && clickStarted && !buttonReleased && gameState == GameState.Playing) {
                        buttonReleased = true;
                        clickStarted = false;
                    }
                    if (buttonReleased && gameState == GameState.Playing) {
                        isBlinking = isCorrectNumber;
                        isShrinking = !isCorrectNumber;

                        // Increment 5: play sound effect
                        //sound.Play((float) 0.2, 0, 0);
                        if (isBlinking)
                            return GameState.Ending;
                    }
                    sourceRectangle.X = currentTexture.Width / 2;
                }
                else {
                    clickStarted = false;
                    buttonReleased = false;
                    sourceRectangle.X = 0;
                }
            }

            // if we get here, return false
            if (!isVisible && isCorrectNumber)
                return GameState.Ended;

            return gameState;
        }

        /// <summary>
        /// Draws the number tile
        /// </summary>
        /// <param name="spriteBatch">the SpriteBatch to use for the drawing</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            // Increments 3, 4, and 5: draw the tile
            if (isVisible)
                spriteBatch.Draw(currentTexture, drawRectangle, sourceRectangle, Color.White);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Loads the content for the tile
        /// </summary>
        /// <param name="contentManager">the content manager</param>
        /// <param name="number">the tile number</param>
        private void LoadContent(ContentManager contentManager, int number)
        {
            // convert the number to a string
            string numberString = ConvertIntToString(number);

            // Increment 3: load content for the tile and set source rectangle
            normalTexture = contentManager.Load<Texture2D>(@"graphics\" + ConvertIntToString(number));
            sourceRectangle = new Rectangle(0, 0, normalTexture.Width / 2, normalTexture.Height);

            // Increment 5: load blinking tile normalTexture
            blinkingTexture = contentManager.Load<Texture2D>(@"graphics\blinking" + ConvertIntToString(number));

            // Increment 5: set current normalTexture
            currentTexture = normalTexture;
        }

        /// <summary>
        /// Converts an integer to a string for the corresponding number
        /// </summary>
        /// <param name="number">the integer to convert</param>
        /// <returns>the string for the corresponding number</returns>
        private String ConvertIntToString(int number)
        {
            switch (number)
            {
                case 1:
                    return "one";
                case 2:
                    return "two";
                case 3:
                    return "three";
                case 4:
                    return "four";
                case 5:
                    return "five";
                case 6:
                    return "six";
                case 7:
                    return "seven";
                case 8:
                    return "eight";
                case 9:
                    return "nine";
                default:
                    throw new Exception("Unsupported number for number tile");
            }
        }

        /// <summary>
        /// Converts an integer to the corresponding keyboard key
        /// </summary>
        /// <param name="number">the integer to convert</param>
        /// <returns>the keyboard key for the corresponding number</returns>
        private Keys ConvertIntToKeyboardKey(int number)
        {
            switch (number)
            {
                case 1:
                    return Keys.NumPad1;
                case 2:
                    return Keys.NumPad2;
                case 3:
                    return Keys.NumPad3;
                case 4:
                    return Keys.NumPad4;
                case 5:
                    return Keys.NumPad5;
                case 6:
                    return Keys.NumPad6;
                case 7:
                    return Keys.NumPad7;
                case 8:
                    return Keys.NumPad8;
                case 9:
                    return Keys.NumPad9;
                default:
                    throw new Exception("Unsupported number for number tile");
            }
        }

        #endregion
    }
}
