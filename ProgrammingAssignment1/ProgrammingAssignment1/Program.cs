﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgrammingAssignment1
{
    /// <summary>
    /// This is the main class for the current Program
    /// </summary>
    class Program
    {
        /// <summary>
        /// A program that asks the user for two points and then calculates the
        /// distance and the angle between those two points before displaying it
        /// in the console
        /// </summary>
        /// <param name="args">Arguments passed when running the program</param>
        static void Main(string[] args)
        {
            // Welcome the user and tell him what this program does
            Console.WriteLine("Welcome to this program. You'll be asked to input two points.");
            Console.WriteLine();
            Console.WriteLine("The program will then determine :");
            Console.WriteLine("  - the distance between those two points");
            Console.WriteLine("  - the angle between those two points");
            Console.WriteLine();

            // Prompt the user for the x and y values of the first point
            Console.WriteLine("First point :");
            Console.Write("Input the x value : ");
            float point1X = float.Parse(Console.ReadLine());
            Console.Write("Input the y value : ");
            float point1Y = float.Parse(Console.ReadLine());
            Console.WriteLine("First point placed at (" + point1X + "," + point1Y + ").");
            Console.WriteLine();

            // Prompt the user for the x and y values of the second point
            Console.WriteLine("Second point :");
            Console.Write("Input the x value : ");
            float point2X = float.Parse(Console.ReadLine());
            Console.Write("Input the y value : ");
            float point2Y = float.Parse(Console.ReadLine());
            Console.WriteLine("Second point placed at (" + point2X + "," + point2Y + ").");
            Console.WriteLine();

            // Determine the x and y deltas between the two points
            float deltaX = point2X - point1X;
            float deltaY = point2Y - point1Y;

            // Determine the distance between the two points using the Pythagorean Theorem
            double distance = Math.Sqrt(Math.Pow(deltaX, 2) + Math.Pow(deltaY, 2));
            Console.WriteLine("The distance between points (" + point1X + "," + point1Y + ")" +
                " and (" + point2X + "," + point2Y + ") is " + distance.ToString("f3") + ".");

            // Determine the angle between the two points using Atan
            double angle = Math.Atan2(deltaX, deltaY);
            double angleInDegrees = angle * 180 / Math.PI;
            Console.WriteLine("The angle between points (" + point1X + "," + point1Y + ")" +
                " and (" + point2X + "," + point2Y + ") is " + angleInDegrees.ToString("f3") + "°.");

            Console.WriteLine();
        }
    }
}
